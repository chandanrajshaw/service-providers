package kashyap.chandan.wepeople;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.wepeople.ServiceProvider.WorkerRegistration;
import kashyap.chandan.wepeople.customer.CustomerRegistrationResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
ImageView arrowGoBack;
TextView btnRegister;
    private final static int ALL_PERMISSIONS_RESULT = 101;
    FusedLocationProviderClient mFusedLocationClient;
    private double lng;
    private double lat;
TextInputEditText etFName,etLName,etmobile,etemail,etAddress,etDescription,etpassword,etconfirmPassword;
Dialog progressDialog;
    private String address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        arrowGoBack.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        if (!checkPermissions())
            requestPermissions();
        else {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(MainActivity.this);
            getLastLocation();
        }
    }

    private void init() {
        arrowGoBack=findViewById(R.id.arrowGoBack);
        btnRegister=findViewById(R.id.btnCustomerRegister);
        etAddress=findViewById(R.id.etAddress);
        etconfirmPassword=findViewById(R.id.etconfirmPassword);
        etpassword=findViewById(R.id.etpassword);
        etDescription=findViewById(R.id.etDescription);
        etemail=findViewById(R.id.etemail);
        etmobile=findViewById(R.id.etmobile);
        etFName=findViewById(R.id.etFName);
        etLName=findViewById(R.id.etLName);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.arrowGoBack:
                finish();
                break;
            case R.id.btnCustomerRegister:
                register();
                break;
        }
    }

    private void register() {
        String fName=etFName.getText().toString().trim();
        String lName=etLName.getText().toString();
        String phone=etmobile.getText().toString();
        String email=etemail.getText().toString();
        String address=etAddress.getText().toString();
        String Description=etDescription.getText().toString();
        String pass=etpassword.getText().toString();
        String conPass=etconfirmPassword.getText().toString();
        if (fName.isEmpty()&&lName.isEmpty()&&phone.isEmpty()&&email.isEmpty()&&address.isEmpty()&&Description.isEmpty()
                &&pass.isEmpty()&&conPass.isEmpty())
            Toast.makeText(this, "Enter All the Fields", Toast.LENGTH_SHORT).show();
        else if (fName.isEmpty())
            Toast.makeText(this, "Enter Your First Name", Toast.LENGTH_SHORT).show();
        else if (lName.isEmpty())
            Toast.makeText(this, "Enter Last Name", Toast.LENGTH_SHORT).show();
        else if (phone.isEmpty()||phone.length()!=10)
            Toast.makeText(this, "Enter Valid Phone Number", Toast.LENGTH_SHORT).show();
        else if (email.isEmpty())
            Toast.makeText(this, "Please Enter Valid Mail", Toast.LENGTH_SHORT).show();
        else if (Description.isEmpty())
            Toast.makeText(this, "Enter Description Field", Toast.LENGTH_SHORT).show();
        else if (pass.isEmpty())
            Toast.makeText(this, "Enter Password", Toast.LENGTH_SHORT).show();
        else if (conPass.isEmpty())
            Toast.makeText(this, "Confirm Password", Toast.LENGTH_SHORT).show();
        else if (!pass.equals(conPass))
            Toast.makeText(this, "Password Miss Match,Enter Both Password Field Carefully", Toast.LENGTH_SHORT).show();
        else
        {
            String addressline[]= TextUtils.split(address,",");
            String area=addressline[0];
            String city=addressline[1];
            String state=addressline[2];
            String country=addressline[3];
            Toast.makeText(this, ""+area+"\n"+city+"\n"+state+"\n"+country, Toast.LENGTH_SHORT).show();
            progressDialog=new Dialog(MainActivity.this);
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            RequestInterface requestInterface= ApiClient.getClient().create(RequestInterface.class);
            Call<CustomerRegistrationResponse>call=requestInterface.userRegistration(fName,lName,phone,email,Description,
                    area,city,state,country,conPass);
            call.enqueue(new Callback<CustomerRegistrationResponse>() {
                @Override
                public void onResponse(Call<CustomerRegistrationResponse> call, Response<CustomerRegistrationResponse> response) {
                    if (response.code()==200)
                    {
                        progressDialog.dismiss();
                        String id=response.body().getUser_id();
                        Toast.makeText(MainActivity.this, "Registration Successfull", Toast.LENGTH_SHORT).show();
                        Intent otpVerificationIntent=new Intent(MainActivity.this, OtpVerification.class);
                        otpVerificationIntent.putExtra("id",id);
                        startActivity(otpVerificationIntent);
                    }
                    else
                    {
                        progressDialog.dismiss();
                        Converter<ResponseBody, ApiError> converter =
                                ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                        ApiError error;
                        try {
                            error = converter.convert(response.errorBody());
                            ApiError.StatusBean status=error.getStatus();
                            Toast.makeText(MainActivity.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) { e.printStackTrace(); }
                    }
                }

                @Override
                public void onFailure(Call<CustomerRegistrationResponse> call, Throwable t) {
progressDialog.dismiss();
                    Toast.makeText(MainActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case ALL_PERMISSIONS_RESULT:
                int length= grantResults.length;
                if (grantResults.length>0)
                {
                    for (int i=0;i<length;i++)
                    {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            // Granted. Start getting the location information
                        }
                        else if(grantResults[i]==PackageManager.PERMISSION_DENIED) {
                            {
                                final AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this,R.style.AlertDialogTheme);
                                builder.setTitle("Notice");
                                builder.setMessage("You Had Denied the Necessary Permissions.Please Go to app Setting and give All the permissions");
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent=new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                                        intent.setData(Uri.parse("package:" +getApplicationContext().getPackageName()));
                                        startActivity(intent);
                                    }
                                });
                                builder.show();
                                break;
                            }

                        }

                    }

                }
                else {
                    getLastLocation();
                }
        }
    }
    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }
    @SuppressLint("MissingPermission")
    private void getLastLocation(){
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {
                                    lat=location.getLatitude();
                                    lng=location.getLongitude();
                                    Toast.makeText(MainActivity.this, lat+" "+lng, Toast.LENGTH_SHORT).show();
                                    LocationAddress locationAddress = new LocationAddress();
                                    locationAddress.getAddressFromLocation(location.getLatitude(), location.getLongitude(),
                                            MainActivity.this, new GeocoderHandler());
                                }
                            }
                        }
                );
            } else {
                Toast.makeText(MainActivity.this, "Turn on location", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                MainActivity.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                ALL_PERMISSIONS_RESULT
        );
    }
    @SuppressLint("MissingPermission")
    private void requestNewLocationData(){

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(MainActivity.this);
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }
    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            lat=mLastLocation.getLatitude();
            lng=mLastLocation.getLongitude();

        }
    };
    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }
    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
            address=locationAddress;
            etAddress.setText(locationAddress);
        }
    }
}