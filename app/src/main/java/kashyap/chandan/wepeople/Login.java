package kashyap.chandan.wepeople;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.wepeople.Admin.AdminDashBoard;
import kashyap.chandan.wepeople.ServiceProvider.ProviderDashBoard;
import kashyap.chandan.wepeople.ServiceProvider.WorkerRegistration;
import kashyap.chandan.wepeople.customer.CustomerDashBoard;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class Login extends AppCompatActivity implements View.OnClickListener {
ImageView arrowGoBack;
    TextView userRegistration,providerRegistration,btnLogin,forgetPassword;
TextInputEditText etmobile,etpassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        arrowGoBack.setOnClickListener(this);
        userRegistration.setOnClickListener(this);
        providerRegistration.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        forgetPassword.setOnClickListener(this);
    }

    private void init() {
        forgetPassword=findViewById(R.id.forgetPassword);
        arrowGoBack=findViewById(R.id.arrowGoBack);
        userRegistration=findViewById(R.id.userRegistration);
        providerRegistration=findViewById(R.id.providerRegistration);
        etmobile=findViewById(R.id.etmobile);
        etpassword=findViewById(R.id.etpassword);
        btnLogin=findViewById(R.id.btnLogin);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.arrowGoBack:
                finish();
                break;
            case R.id.userRegistration:
                Intent userRegistrationIntent=new Intent(Login.this,MainActivity.class);
                startActivity(userRegistrationIntent);
                break;
            case R.id.providerRegistration:
                Intent providerRegistrationIntent=new Intent(Login.this, WorkerRegistration.class);
                startActivity(providerRegistrationIntent);
                break;
            case R.id.btnLogin:
                login();
                break;
            case R.id.forgetPassword:
                Intent forgetPasswordIntent=new Intent(Login.this, ForgetPassword.class);
                startActivity(forgetPasswordIntent);
                break;
        }
    }
    private void login()
    {
        String mobile=etmobile.getText().toString();
        final String password=etpassword.getText().toString().trim();
        if (mobile.isEmpty()&&mobile.length()!=10&&password.isEmpty())
            Snackbar.make(Login.this.getWindow().getDecorView().findViewById(android.R.id.content),"Please Fill All Fields",Snackbar.LENGTH_LONG).show();
        else if (mobile.isEmpty()||mobile.length()!=10)
            Snackbar.make(Login.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter Valid Phone Number",Snackbar.LENGTH_LONG).show();
        else if (password.isEmpty())
            Snackbar.make(Login.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter Password",Snackbar.LENGTH_LONG).show();
        else
        {
            final Dialog progressDialog=new Dialog(Login.this);
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            RequestInterface requestInterface= ApiClient.getClient().create(RequestInterface.class);
            Call<LoginResponse>call=requestInterface.login(mobile,password);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
if (response.code()==200)
{
progressDialog.dismiss();
LoginResponse.DataBean data=response.body().getData();
String role=data.getRole();
SharedPreferences preferences=new SharedPreferences(Login.this);
String imageurl=data.getImage();
preferences.putSharedPreference(data.getId(),data.getFirst_name(),data.getLast_name(),password,data.getPhone()
,data.getEmail(),data.getImage(),data.getArea(),data.getCity_name(),data.getState_name(),data.getCountry_name(),role,true);
if (role.equalsIgnoreCase("service_provider"))
{
    Intent providerIntent=new Intent(Login.this, ProviderDashBoard.class);
    Bundle bundle=new Bundle();

    bundle.putSerializable("data",data);
    providerIntent.putExtra("bundle",bundle);

    providerIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(providerIntent);
}
  else   if (role.equalsIgnoreCase("user"))
    {
        Intent providerIntent=new Intent(Login.this, CustomerDashBoard.class);
        Bundle bundle=new Bundle();
        bundle.putSerializable("data",data);
        providerIntent.putExtra("bundle",bundle);
        providerIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(providerIntent);
    }
else {
    Snackbar.make(Login.this.getWindow().getDecorView().findViewById(android.R.id.content),"User Type Error",Snackbar.LENGTH_SHORT).show();
}
}
else if (response.code()!=200){
    progressDialog.dismiss();
    Converter<ResponseBody, ApiError> converter =
            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
    ApiError error;
    try {
        error = converter.convert(response.errorBody());
        ApiError.StatusBean status=error.getStatus();
        int code=status.getCode();
        Toast.makeText(Login.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
        if (code==402)
        {
            String userId=error.getUser_id();
            Intent otpVerificationIntent=new Intent(Login.this,OtpVerification.class);
            otpVerificationIntent.putExtra("id",userId);
            startActivity(otpVerificationIntent);
        }
        else
        {

        }
    } catch (IOException e) { e.printStackTrace(); }
} }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(Login.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}