package kashyap.chandan.wepeople;

public class ApiError {
    private StatusBean status;
    private String user_id;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public static class StatusBean {
        /**
         * code : 402
         * message : Sorry! Your Email no is not varified Please verify it by otp sent to your registed email !
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
