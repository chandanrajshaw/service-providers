package kashyap.chandan.wepeople;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.wepeople.ServiceProvider.WorkerRegistration;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class OtpVerification extends AppCompatActivity implements View.OnClickListener {
    ImageView arrowGoBack;
    TextInputEditText etOtp;
    Intent intent;
    TextView btnsubmitOtp;
    Dialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        init();
        intent=getIntent();
        arrowGoBack.setOnClickListener(this);
        btnsubmitOtp.setOnClickListener(this);
    }

    private void init() {
        arrowGoBack=findViewById(R.id.arrowGoBack);
        etOtp=findViewById(R.id.etOtp);
        btnsubmitOtp=findViewById(R.id.btnsubmitOtp);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.arrowGoBack:
                finish();
                break;
            case R.id.btnsubmitOtp:
               submitOtp();
                break;

        }
    }

    private void submitOtp() {
      String otp=etOtp.getText().toString().trim();
        progressDialog=new Dialog(OtpVerification.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        RequestInterface requestInterface= ApiClient.getClient().create(RequestInterface.class);
        Call<OtpVerificationResponse>call=requestInterface.accountVerification(otp,intent.getStringExtra("id"));
        call.enqueue(new Callback<OtpVerificationResponse>() {
            @Override
            public void onResponse(Call<OtpVerificationResponse> call, Response<OtpVerificationResponse> response) {
                if (response.code() == 200)
                {
                    progressDialog.dismiss();
                    Toast.makeText(OtpVerification.this, "Verified!!! Login Now", Toast.LENGTH_SHORT).show();
              Intent intent=new Intent(OtpVerification.this,Login.class);
              intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
              startActivity(intent);
                }
                else
                {
                    progressDialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(OtpVerification.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<OtpVerificationResponse> call, Throwable t) {
progressDialog.dismiss();
                Toast.makeText(OtpVerification.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}