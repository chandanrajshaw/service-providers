package kashyap.chandan.wepeople;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Api;
import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ForgetPassword extends AppCompatActivity implements View.OnClickListener {
    LinearLayout otplayout;
    TextView btnGetOtp,btnsubmitOtp;
    ImageView goBack;
    Dialog progressDialog;
    TextInputEditText etMobile,etOtp,etNewPassword,etConfirmPassword;
    String id;;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        init();
        goBack.setOnClickListener(this);
        btnGetOtp.setOnClickListener(this);
        btnsubmitOtp.setOnClickListener(this);

    }

    private void init() {
        otplayout=findViewById(R.id.otplayout);
        btnGetOtp=findViewById(R.id.btnGetOtp);
        goBack=findViewById(R.id.goBack);
        etMobile=findViewById(R.id.etMobile);
        btnsubmitOtp=findViewById(R.id.btnsubmitOtp);
        etOtp=findViewById(R.id.etOtp);
        etNewPassword=findViewById(R.id.etNewPassword);
        etConfirmPassword=findViewById(R.id.etConfirmPassword);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.goBack:
                finish();
                break;
            case R.id.btnGetOtp:
                sendOtp();
                break;
            case R.id.btnsubmitOtp:
                resetPassword();
                break;
        }
    }

    private void resetPassword() {
        String otp=etOtp.getText().toString();
        String newPass=etNewPassword.getText().toString().trim();
        String conPass=etConfirmPassword.getText().toString().trim();
        if (otp.isEmpty()&&newPass.isEmpty()&&conPass.isEmpty())
        {
            Toast.makeText(ForgetPassword.this, "Fill All the fields", Toast.LENGTH_SHORT).show();
        }
        else if (otp.isEmpty())
            Toast.makeText(ForgetPassword.this, "Enter Otp", Toast.LENGTH_SHORT).show();
        else if (newPass.isEmpty())
            Toast.makeText(ForgetPassword.this, "Enter New Password", Toast.LENGTH_SHORT).show();
        else if (conPass.isEmpty())
            Toast.makeText(ForgetPassword.this, "Enter Confirm Password", Toast.LENGTH_SHORT).show();
        else if (!newPass.equals(conPass))
            Toast.makeText(ForgetPassword.this, "Plzz Enter Confirm Password Carefully", Toast.LENGTH_SHORT).show();
        else
        {
            progressDialog=new Dialog(ForgetPassword.this);
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            RequestInterface requestInterface= ApiClient.getClient().create(RequestInterface.class);
            Call<ResetPasswordResponse>call=requestInterface.resetPassword(id,otp,newPass);
            call.enqueue(new Callback<ResetPasswordResponse>() {
                @Override
                public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                    if (response.code()==200)
                    {
                        progressDialog.dismiss();
                        Toast.makeText(ForgetPassword.this, "Password Changed Successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else {
                        progressDialog.dismiss();
                        Converter<ResponseBody, ApiError> converter = ApiClient.getClient().
                                responseBodyConverter(ApiError.class,new Annotation[0]);
                        ApiError error;
                        try {
                            error = converter.convert(response.errorBody());
                            ApiError.StatusBean status=error.getStatus();
                            Toast.makeText(ForgetPassword.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) { e.printStackTrace(); }
                    }
                }

                @Override
                public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
progressDialog.dismiss();
                    Toast.makeText(ForgetPassword.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }



    }

    private void sendOtp() {
        String mobile=etMobile.getText().toString();
        if (mobile.isEmpty()||mobile.length()!=10)
            Toast.makeText(ForgetPassword.this, "Enter Valid Registered Mobile Number", Toast.LENGTH_SHORT).show();
    else
        {
            progressDialog=new Dialog(ForgetPassword.this);
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            RequestInterface requestInterface= ApiClient.getClient().create(RequestInterface.class);
            Call<ForgetPasswordResponse>call=requestInterface.forgetPassword(mobile);
            call.enqueue(new Callback<ForgetPasswordResponse>() {
                @Override
                public void onResponse(Call<ForgetPasswordResponse> call, Response<ForgetPasswordResponse> response) {
                    if (response.code()==200)
                    {
                        progressDialog.dismiss();
                        id=response.body().getUser_id();
                        otplayout.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        progressDialog.dismiss();
                        Converter<ResponseBody, ApiError> converter = ApiClient.getClient().
                                responseBodyConverter(ApiError.class,new Annotation[0]);
                        ApiError error;
                        try {
                            error = converter.convert(response.errorBody());
                            ApiError.StatusBean status=error.getStatus();
                            Toast.makeText(ForgetPassword.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) { e.printStackTrace(); }
                    }
                }

                @Override
                public void onFailure(Call<ForgetPasswordResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(ForgetPassword.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } }
}