package kashyap.chandan.wepeople;

import java.io.Serializable;
import java.util.List;

public class ServiceTypeResponse implements Serializable {

    /**
     * status : {"code":200,"message":"List Of active Service types"}
     * data : [{"id":"1","type":"Hindi","status":"1"},{"id":"2","type":"servent","status":"1"},{"id":"3","type":"Cobler","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable{
        /**
         * code : 200
         * message : List Of active Service types
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable{
        /**
         * id : 1
         * type : Hindi
         * status : 1
         */

        private String id;
        private String type;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
