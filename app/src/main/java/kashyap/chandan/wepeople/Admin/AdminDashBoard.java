package kashyap.chandan.wepeople.Admin;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.wepeople.ApiClient;
import kashyap.chandan.wepeople.LoginResponse;
import kashyap.chandan.wepeople.R;
import kashyap.chandan.wepeople.RequestInterface;

public class AdminDashBoard extends AppCompatActivity {
    Toolbar toolbar;
    DrawerLayout drawerlayout;
    TextView proname,phone,tvTitle;
    Intent intent;
    RecyclerView enquiryList;
    CircleImageView   ivProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_dash_board);
        init();
        intent=getIntent();
        Bundle bundle=intent.getBundleExtra("bundle");
        LoginResponse.DataBean userData= (LoginResponse.DataBean) bundle.getSerializable("data");
        tvTitle.setText("Welcome "+userData.getFirst_name());
        proname.setText(userData.getFirst_name()+" "+userData.getLast_name());
        phone.setText(userData.getPhone());
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        toolbar.setNavigationIcon(R.drawable.ic_view_list);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { drawerlayout.openDrawer(Gravity.LEFT); } });

    }

    private void init() {
        ivProfile=findViewById(R.id.ivProfile);
        enquiryList=findViewById(R.id.enquiryList);
        toolbar=findViewById(R.id.toolbar);
        drawerlayout=findViewById(R.id.drawerLayout);
        proname=findViewById(R.id.tvproname);
        phone=findViewById(R.id.adminphone);
        tvTitle=findViewById(R.id.tvTitle);
    }
    void enquiryList()
    {
        RequestInterface requestInterface= ApiClient.getClient().create(RequestInterface.class);

    }
}