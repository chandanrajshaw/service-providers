package kashyap.chandan.wepeople.customer;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.wepeople.ApiClient;
import kashyap.chandan.wepeople.CustomItemClickListener;
import kashyap.chandan.wepeople.Login;
import kashyap.chandan.wepeople.R;
import kashyap.chandan.wepeople.RequestInterface;
import kashyap.chandan.wepeople.SharedPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceProviderAdapter extends RecyclerView.Adapter<ServiceProviderAdapter.MyViewHolder> /*implements Filterable */{
    Context context;
    List<ProviderListResponse.DataBean> providerList;
    CustomItemClickListener listener;
    private int mSelectedItem = -1;
    Dialog progressDialog;
    SharedPreferences preferences;
    public ServiceProviderAdapter(Context context, List<ProviderListResponse.DataBean> providerList, CustomItemClickListener listener) {
        this.context=context;
        this.providerList=providerList;
        this.listener=listener;
        preferences=new SharedPreferences(context);
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.service_provider_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.address.setText(providerList.get(position).getArea()+","+providerList.get(position).getCity_name());
        holder.serviceType.setText(providerList.get(position).getType());
        holder.name.setText(providerList.get(position).getFirst_name()+" "+providerList.get(position).getLast_name());
        holder.cardCall.setOnClickListener(new View.OnClickListener()
        {
    @Override
    public void onClick(View view) {
        int pos=holder.getAdapterPosition();
       enquiry(providerList.get(pos));
    }
});
     holder.detail.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {

             Intent intent=new Intent(context,ProviderDetail.class);
             Bundle bundle=new Bundle();
             bundle.putSerializable("data",providerList.get(position));
             intent.putExtra("bundle",bundle);
             context.startActivity(intent);
         }
     });
    }

    @Override
    public int getItemCount() {
        return providerList.size();
    }

//    @Override
//    public Filter getFilter() {
//        return new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence charSequence) {
//                final FilterResults oReturn = new FilterResults();
//                List<ProviderListResponse.DataBean> filteredList = new ArrayList<>();
//                if (charSequence == null || charSequence.length() == 0) {
//                    filteredList.addAll(providerList);
//                }
//                else {
//                    String filterPattern = charSequence.toString().toLowerCase().trim();
//                    for (ProviderListResponse.DataBean item : providerList) {
//                        if (item.getCity_name().toLowerCase().contains(filterPattern)) {
//                            filteredList.add(item);
//                        }
//                        else if (item.getArea().toLowerCase().contains(filterPattern))
//                        {
//                            filteredList.add(item);
//                        }
//
//                    }
//                }
//                FilterResults results = new FilterResults();
//                results.values = filteredList;
//                return results;
//            }
//
//            @Override
//            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//                providerList.clear();
//                providerList.addAll((List) filterResults.values);
//                notifyDataSetChanged();
//            }
//        };
//    }

    public void setFilter(ArrayList<ProviderListResponse.DataBean> filteredList) {
        providerList = new ArrayList<>();
        providerList.addAll(filteredList);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CardView detail,cardCall;
        TextView address,serviceType,name;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            detail=itemView.findViewById(R.id.view);
            name=itemView.findViewById(R.id.name);
            address=itemView.findViewById(R.id.address);
            serviceType=itemView.findViewById(R.id.serviceType);
            cardCall=itemView.findViewById(R.id.cardCall);
            View.OnClickListener clickListener=new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSelectedItem=getAdapterPosition();
                    notifyDataSetChanged();
                    listener.onItemClick(view,providerList.get(mSelectedItem).getId(),providerList.get(mSelectedItem).getFirst_name()+" "+providerList.get(mSelectedItem).getLast_name());
                }
            };
            itemView.setOnClickListener(clickListener);
        }
    }
    private void enquiry(final ProviderListResponse.DataBean data)
    {
        progressDialog=new Dialog(context);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        RequestInterface requestInterface= ApiClient.getClient().create(RequestInterface.class);
        Call<EnquiryResponse> call=requestInterface.makeEnquiry(preferences.getId(),data.getId());
        call.enqueue(new Callback<EnquiryResponse>() {
            @Override
            public void onResponse(Call<EnquiryResponse> call, Response<EnquiryResponse> response) {
                if (response.code()==200)
                {
                    String mob=data.getPhone();
                    Uri u = Uri.fromParts("tel",mob,null);
                    Intent i = new Intent(Intent.ACTION_DIAL, u);
                    context.startActivity(i);
                    progressDialog.dismiss();

                }
                else
                {
                    progressDialog.dismiss();
                    Toast.makeText(context, "Unable To Call", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<EnquiryResponse> call, Throwable t) {
progressDialog.dismiss();
                Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
