package kashyap.chandan.wepeople.customer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.wepeople.R;

public class ProviderDetail extends AppCompatActivity {
TextView tvServiceType,tvPhone,tvMail,tvAddress,tvProviderName;
CircleImageView profileImage;
ImageView arrowGoBack;
Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_detail);
        init();
        intent=getIntent();
        Bundle bundle=intent.getBundleExtra("bundle");
        ProviderListResponse.DataBean data= (ProviderListResponse.DataBean) bundle.getSerializable("data");
        Picasso.get().load("http://www.igranddeveloper.xyz/sprovider/"+data.getImage()).placeholder(R.drawable.loading).error(R.drawable.servicelogo).into(profileImage);
tvProviderName.setText(data.getFirst_name()+" "+data.getLast_name());
tvAddress.setText(data.getArea()+","+data.getCity_name()+","+data.getState_name()+","+data.getCountry_name());
tvMail.setText(data.getEmail());
tvPhone.setText(data.getPhone());
tvServiceType.setText(data.getType());

        arrowGoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void init() {
        arrowGoBack=findViewById(R.id.arrowGoBack);
        profileImage=findViewById(R.id.profileImage);
        tvProviderName=findViewById(R.id.tvProviderName);
        tvServiceType=findViewById(R.id.tvServiceType);
        tvPhone=findViewById(R.id.tvPhone);
        tvMail=findViewById(R.id.tvMail);
        tvAddress=findViewById(R.id.tvAddress)  ;
    }
}