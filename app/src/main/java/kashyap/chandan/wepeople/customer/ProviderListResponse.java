package kashyap.chandan.wepeople.customer;

import java.io.Serializable;
import java.util.List;

public class ProviderListResponse implements Serializable {


    /**
     * status : {"code":200,"message":"Provider list"}
     * data : [{"id":"3","first_name":"asha","last_name":"Rani","email":"asha@gmail.com","phone":"9931558107","pass":"$2y$10$nVxTopdCjd0vMJdKXdJHCem.Oa2tCfy0q3CAgD2s69waTM1akcYxa","uid":"26qn7kj8v9dm5io","image":"","otp":"265311","role":"service_provider","type_id":"1","description":"                sd","area":"sas","city_name":"Hazaribag","state_name":"JHARKHAND","country_name":"India","status":"1","type":"Hindi"},{"id":"4","first_name":"chandan","last_name":"Kumar","email":"shruti.chandan.kashyap@gmail.com","phone":"8789160492","pass":"$2y$10$fsjpv8j6RaCel8LQodhxEefMmib2NxYETluFLutQHQ/YZeBLMCg3e","uid":"c7zjl4n9pd10m3w","image":"","otp":"624044","role":"service_provider","type_id":"1","description":"Tailor","area":"Sindoor Hazaribagh","city_name":"Hazaribagh","state_name":"Jharkhand","country_name":"India","status":"1","type":"Hindi"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Provider list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 3
         * first_name : asha
         * last_name : Rani
         * email : asha@gmail.com
         * phone : 9931558107
         * pass : $2y$10$nVxTopdCjd0vMJdKXdJHCem.Oa2tCfy0q3CAgD2s69waTM1akcYxa
         * uid : 26qn7kj8v9dm5io
         * image :
         * otp : 265311
         * role : service_provider
         * type_id : 1
         * description :                 sd
         * area : sas
         * city_name : Hazaribag
         * state_name : JHARKHAND
         * country_name : India
         * status : 1
         * type : Hindi
         */

        private String id;
        private String first_name;
        private String last_name;
        private String email;
        private String phone;
        private String pass;
        private String uid;
        private String image;
        private String otp;
        private String role;
        private String type_id;
        private String description;
        private String area;
        private String city_name;
        private String state_name;
        private String country_name;
        private String status;
        private String type;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPass() {
            return pass;
        }

        public void setPass(String pass) {
            this.pass = pass;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getType_id() {
            return type_id;
        }

        public void setType_id(String type_id) {
            this.type_id = type_id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public String getState_name() {
            return state_name;
        }

        public void setState_name(String state_name) {
            this.state_name = state_name;
        }

        public String getCountry_name() {
            return country_name;
        }

        public void setCountry_name(String country_name) {
            this.country_name = country_name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
