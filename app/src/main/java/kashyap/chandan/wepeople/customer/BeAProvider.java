package kashyap.chandan.wepeople.customer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.wepeople.ApiClient;
import kashyap.chandan.wepeople.ApiError;
import kashyap.chandan.wepeople.CustomItemClickListener;
import kashyap.chandan.wepeople.Login;
import kashyap.chandan.wepeople.OtpVerification;
import kashyap.chandan.wepeople.R;
import kashyap.chandan.wepeople.RequestInterface;
import kashyap.chandan.wepeople.ServiceProvider.ProviderRegistrationResponse;
import kashyap.chandan.wepeople.ServiceProvider.ServiceTypeAdapter;
import kashyap.chandan.wepeople.ServiceProvider.WorkerRegistration;
import kashyap.chandan.wepeople.ServiceTypeResponse;
import kashyap.chandan.wepeople.SharedPreferences;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class BeAProvider extends AppCompatActivity implements View.OnClickListener{
    ImageView arrowGoBack;
    TextView btnProvider,etServiceType;
    LinearLayout serviceLayout;
    Dialog progressDialog,serviceDialog;
    RecyclerView serviceRecycler;
    TextInputEditText etDescription;
     String serviceTypeId;
     SharedPreferences preferences;
     CircleImageView myImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_be_a_provider);
        init();
        Picasso.get().load("http://www.igranddeveloper.xyz/sprovider/"+preferences.getImage()).placeholder(R.drawable.loading).error(R.drawable.servicelogo).into(myImage);
        serviceLayout.setOnClickListener(this);
        arrowGoBack.setOnClickListener(this);
        btnProvider.setOnClickListener(this);
        etServiceType.setOnClickListener(this);
    }

    private void init() {
        myImage=findViewById(R.id.myImage);
        etServiceType=findViewById(R.id.etServiceType);
        serviceLayout=findViewById(R.id.serviceLayout);
        arrowGoBack=findViewById(R.id.arrowGoBack);
        btnProvider=findViewById(R.id.btnProvider);
        etDescription=findViewById(R.id.etDescription);
        preferences=new SharedPreferences(BeAProvider.this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.etServiceType:
                serviceType();
                break;
            case R.id.arrowGoBack:
                finish();
                break;
            case R.id.btnProvider:
                becomeProvider();
                break;
        }
    }
    void serviceType()
    {
        progressDialog=new Dialog(BeAProvider.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        RequestInterface requestInterface= ApiClient.getClient().create(RequestInterface.class);
        Call<ServiceTypeResponse> call=requestInterface.serviceTypeList();
        call.enqueue(new Callback<ServiceTypeResponse>() {
            @Override
            public void onResponse(Call<ServiceTypeResponse> call, Response<ServiceTypeResponse> response) {
                if (response.code()==200)
                {
                    progressDialog.dismiss();
                    List<ServiceTypeResponse.DataBean> serviceTypeList=response.body().getData();
                    serviceDialog=new Dialog(BeAProvider.this);
                    serviceDialog.setContentView(R.layout.dropdowndialog);
                    DisplayMetrics metrics = getResources().getDisplayMetrics();
                    int width = metrics.widthPixels;
                    serviceDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                    serviceRecycler = serviceDialog.findViewById(R.id.recycleroption);
                    serviceRecycler.setLayoutManager(new LinearLayoutManager(BeAProvider.this, LinearLayoutManager.VERTICAL, false));
                    serviceRecycler.setAdapter(new ServiceTypeAdapter(BeAProvider.this,serviceTypeList,serviceDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, String id, String value) {
                            etServiceType.setText(value);
                            serviceTypeId=id;
                        }
                    }
                    ));
                    serviceDialog.show();
                }
                else
                {
                    progressDialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(BeAProvider.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<ServiceTypeResponse> call, Throwable t) {
                progressDialog.dismiss();
                Snackbar.make(BeAProvider.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();

            }
        });
    }
    void becomeProvider()
    {

        String Description=etDescription.getText().toString();

        if (serviceTypeId==null&&Description.isEmpty())
            Toast.makeText(this, "Enter All the Fields", Toast.LENGTH_SHORT).show();
        else if (serviceTypeId==null)
            Toast.makeText(this, "Select Service Type", Toast.LENGTH_SHORT).show();
        else if (Description.isEmpty())
            Toast.makeText(this, "Enter Description Field", Toast.LENGTH_SHORT).show();
        else
        {

            progressDialog=new Dialog(BeAProvider.this);
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            RequestInterface requestInterface= ApiClient.getClient().create(RequestInterface.class);
            Call<BecomeProviderResponse>call=requestInterface.becomeProvider(preferences.getId(),serviceTypeId,Description);
            call.enqueue(new Callback<BecomeProviderResponse>() {
                @Override
                public void onResponse(Call<BecomeProviderResponse> call, Response<BecomeProviderResponse> response) {
                    if (response.code() == 200)
                    {
                        progressDialog.dismiss();
                        Toast.makeText(BeAProvider.this, "Successfully Changed To Service Provider", Toast.LENGTH_SHORT).show();
                        preferences.sessionEnd();
                        Intent intent=new Intent(BeAProvider.this, Login.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                    else
                    {
                        progressDialog.dismiss();
                        Converter<ResponseBody, ApiError> converter =
                                ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                        ApiError error;
                        try {
                            error = converter.convert(response.errorBody());
                            ApiError.StatusBean status=error.getStatus();
                            Toast.makeText(BeAProvider.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) { e.printStackTrace(); }
                    }
                }

                @Override
                public void onFailure(Call<BecomeProviderResponse> call, Throwable t) {

                }
            });

        }
    }
}