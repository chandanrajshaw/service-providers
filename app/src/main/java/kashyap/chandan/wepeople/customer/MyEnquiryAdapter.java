package kashyap.chandan.wepeople.customer;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.wepeople.CustomItemClickListener;
import kashyap.chandan.wepeople.R;

public class MyEnquiryAdapter extends RecyclerView.Adapter<MyEnquiryAdapter.MyViewHolder> {
    Context context;
    List<MyEnquiryResponse.DataBean> dataList;
    CustomItemClickListener listener;
    private int mSelectedItem = -1;
    public MyEnquiryAdapter(Context context, List<MyEnquiryResponse.DataBean> dataList, CustomItemClickListener listener) {
    this.context=context;
    this.dataList=dataList;
    this.listener=listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.enquiry_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
    holder.tvProviderName.setText(dataList.get(position).getFirst_name()+" "+dataList.get(position).getLast_name());
    holder.tvProvidingFor.setText(dataList.get(position).getType());
    holder.tvProviderAddress.setText(dataList.get(position).getDate_time());
        holder.callLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos=holder.getAdapterPosition();
                String mob=dataList.get(pos).getPhone();
                Uri u = Uri.fromParts("tel",mob,null);
                Intent i = new Intent(Intent.ACTION_DIAL, u);
                context.startActivity(i);
            }
        });
    }
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CardView callLayout;
        TextView tvProviderName,tvProvidingFor,tvProviderAddress;
        TextView call;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvProviderAddress=itemView.findViewById(R.id.tvProviderAddress);
            tvProvidingFor=itemView.findViewById(R.id.tvProvidingFor);
            tvProviderName=itemView.findViewById(R.id.tvProviderName);
            call=itemView.findViewById(R.id.call);
            callLayout=itemView.findViewById(R.id.callLayout);
            callLayout.setVisibility(View.VISIBLE);
            View.OnClickListener clickListener=new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    listener.onItemClick(view,dataList.get(mSelectedItem).getId(),dataList.get(mSelectedItem).getFirst_name()+""+dataList.get(mSelectedItem).getLast_name());
                }
            };
            itemView.setOnClickListener(clickListener);
        }
    }
}
