package kashyap.chandan.wepeople.customer;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.wepeople.ApiClient;
import kashyap.chandan.wepeople.ApiError;
import kashyap.chandan.wepeople.CustomItemClickListener;
import kashyap.chandan.wepeople.Login;
import kashyap.chandan.wepeople.LoginResponse;
import kashyap.chandan.wepeople.R;
import kashyap.chandan.wepeople.RequestInterface;
import kashyap.chandan.wepeople.ServiceProvider.EditProfile;
import kashyap.chandan.wepeople.SharedPreferences;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class CustomerDashBoard extends AppCompatActivity {
    Toolbar toolbar;
    DrawerLayout drawerlayout;
    TextView proname,phone,tvTitle;
    Intent intent;
    RecyclerView ProviderList;
    CircleImageView   ivProfile,navheaderpropic;
    SharedPreferences preferences;
    Dialog progressDialog;
    private String providerId;
    LinearLayout serviceProviderLayout,logoutLayout,becomeServiceProvider,homeLayout;
    private SearchView searchView;
ServiceProviderAdapter adapter;
SwipeRefreshLayout swipeRefresh;
    List<ProviderListResponse.DataBean> providerList=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_dash_board);
        init();
        intent=getIntent();
        Bundle bundle=intent.getBundleExtra("bundle");
        LoginResponse.DataBean userData= (LoginResponse.DataBean) bundle.getSerializable("data");
        tvTitle.setText("Welcome "+preferences.getFName());
        proname.setText(preferences.getFName()+" "+preferences.getLName());
        phone.setText(preferences.getPhone());
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        toolbar.setNavigationIcon(R.drawable.ic_view_list);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { @Override public void onClick(View v)
        { drawerlayout.openDrawer(Gravity.LEFT); } });
        ivProfile.setVisibility(View.VISIBLE);
        ivProfile.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent editProfile=new Intent(CustomerDashBoard.this, EditProfile.class);
        startActivity(editProfile);
    }
});
        homeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerlayout.closeDrawer(GravityCompat.START);
            }
        });
        serviceProviderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent providerList=new Intent(CustomerDashBoard.this, MyEnquiryList.class);
                startActivity(providerList);
                drawerlayout.closeDrawer(GravityCompat.START);
            }
        });
        logoutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.sessionEnd();
                Intent intent=new Intent(CustomerDashBoard.this,Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        becomeServiceProvider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent becomeProvide=new Intent(CustomerDashBoard.this, BeAProvider.class);
                startActivity(becomeProvide);
                drawerlayout.closeDrawer(GravityCompat.START);
            }
        });

        if (!preferences.getImage().isEmpty())
        {
            Picasso.get().load("http://www.igranddeveloper.xyz/sprovider/"+preferences.getImage()).placeholder(R.drawable.loading).error(R.drawable.servicelogo).into(navheaderpropic);
            Picasso.get().load("http://www.igranddeveloper.xyz/sprovider/"+preferences.getImage()).placeholder(R.drawable.loading).error(R.drawable.servicelogo).into(ivProfile);

        }
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
serviceProviders();
swipeRefresh.setRefreshing(false);
            }
        });
    }

    private void init() {
        swipeRefresh=findViewById(R.id.swipeRefresh);
        homeLayout=findViewById(R.id.homeLayout);
        becomeServiceProvider=findViewById(R.id.becomeServiceProvider);
        navheaderpropic=findViewById(R.id.navheaderpropic);
        ivProfile=findViewById(R.id.ivProfile);
        ProviderList=findViewById(R.id.enquiryList);
        toolbar=findViewById(R.id.toolbar);
        drawerlayout=findViewById(R.id.drawerLayout);
        serviceProviderLayout=findViewById(R.id.serviceProviderLayout);
        proname=findViewById(R.id.tvproname);
        phone=findViewById(R.id.adminphone);
        tvTitle=findViewById(R.id.tvTitle);
        preferences=new SharedPreferences(CustomerDashBoard.this);
        logoutLayout=findViewById(R.id.logoutLayout);
    }

    void serviceProviders()
    {
        progressDialog=new Dialog(CustomerDashBoard.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        RequestInterface requestInterface= ApiClient.getClient().create(RequestInterface.class);
        Call<ProviderListResponse>call=requestInterface.providerList();
        call.enqueue(new Callback<ProviderListResponse>() {


            @Override
            public void onResponse(Call<ProviderListResponse> call, Response<ProviderListResponse> response) {
                if (response.code()==200)
                {
                    progressDialog.dismiss();
                    providerList=response.body().getData();
                    ProviderList.setLayoutManager(new LinearLayoutManager(CustomerDashBoard.this,LinearLayoutManager.VERTICAL,false));
                    adapter=new ServiceProviderAdapter(CustomerDashBoard.this,providerList, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, String id, String value) {
                            providerId=id;

                        }
                    });
                    ProviderList.setAdapter(adapter);
                }
                else
                {
                    progressDialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(CustomerDashBoard.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }
                } }

            @Override
            public void onFailure(Call<ProviderListResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(CustomerDashBoard.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        serviceProviders();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchItem.getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getCallingActivity()));
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                ArrayList<ProviderListResponse.DataBean> filteredList = new ArrayList<>();
                for (ProviderListResponse.DataBean data: providerList){
                    String cityName = data.getCity_name().toLowerCase();
                    String areaName=data.getArea().toLowerCase();
                    String stateName=data.getState_name().toLowerCase();
                    if (cityName.contains(newText))
                    {
                        filteredList.add(data);
                    }
                    else if (areaName.contains(newText))
                    {filteredList.add(data);}
                    else if (stateName.contains(newText))
                    {filteredList.add(data);}
                }
                adapter.setFilter(filteredList);
                return true;
            }
        });
        return true;//oncreateoptionsmenu
    }
}