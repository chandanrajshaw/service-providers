package kashyap.chandan.wepeople.customer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

import kashyap.chandan.wepeople.ApiClient;
import kashyap.chandan.wepeople.ApiError;
import kashyap.chandan.wepeople.CustomItemClickListener;
import kashyap.chandan.wepeople.R;
import kashyap.chandan.wepeople.RequestInterface;
import kashyap.chandan.wepeople.SharedPreferences;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class MyEnquiryList extends AppCompatActivity implements View.OnClickListener {
TextView toolGenHearder;
ImageView toolgoback;
RecyclerView enquiryList ;
Dialog progressDialog;
String providerId;
SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_list);
        init();
        toolGenHearder.setText("My Enquiry");
        toolgoback.setOnClickListener(this);
    }

    private void init() {
        preferences=new SharedPreferences(MyEnquiryList.this);
        toolGenHearder=findViewById(R.id.toolgenheader);
        toolgoback=findViewById(R.id.toolgoback);
        enquiryList=findViewById(R.id.ProviderList);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.toolgoback:
                finish();
                break;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        myEnquiryList();
    }
    void myEnquiryList()
    {
        progressDialog=new Dialog(MyEnquiryList.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        RequestInterface requestInterface= ApiClient.getClient().create(RequestInterface.class);
        Call<MyEnquiryResponse>call=requestInterface.customer_My_Enquiry(preferences.getId());
        call.enqueue(new Callback<MyEnquiryResponse>() {
            @Override
            public void onResponse(Call<MyEnquiryResponse> call, Response<MyEnquiryResponse> response) {
                if (response.code()==200)
                {
                    progressDialog.dismiss();
                    List<MyEnquiryResponse.DataBean> dataList=response.body().getData();
                    enquiryList.setLayoutManager(new LinearLayoutManager(MyEnquiryList.this,LinearLayoutManager.VERTICAL,false));
                    enquiryList.setAdapter(new MyEnquiryAdapter(MyEnquiryList.this,dataList, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, String id, String value) {

                        }
                    }));
                }
                else
                {
                    progressDialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(MyEnquiryList.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<MyEnquiryResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MyEnquiryList.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }
}