package kashyap.chandan.wepeople;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.wepeople.customer.ProviderListResponse;

public class LandingScreenAdapter extends RecyclerView.Adapter<LandingScreenAdapter.MyViewHolder> {
    Context context;
    List<ProviderListResponse.DataBean> providerList;
    CustomItemClickListener listener;
    public LandingScreenAdapter(Context context, List<ProviderListResponse.DataBean> providerList, CustomItemClickListener listener) {
        this.context=context;
        this.providerList=providerList;
        this.listener=listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.service_provider_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.address.setText(providerList.get(position).getArea()+","+providerList.get(position).getCity_name());
        holder.serviceType.setText(providerList.get(position).getType());
        holder.name.setText(providerList.get(position).getFirst_name()+" "+providerList.get(position).getLast_name());
holder.detail.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(context,Login.class);
        context.startActivity(intent);
    }
});
        holder.cardCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context,Login.class);
                context.startActivity(intent);
            }
        });
        holder.cardDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context,Login.class);
                context.startActivity(intent);
            }
        });
        holder.cardCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context,Login.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return providerList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CardView detail,cardCall,cardDetail;
        TextView address,serviceType,name;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            cardDetail=itemView.findViewById(R.id.cardDetail);
            detail=itemView.findViewById(R.id.view);
            name=itemView.findViewById(R.id.name);
            address=itemView.findViewById(R.id.address);
            serviceType=itemView.findViewById(R.id.serviceType);
            cardCall=itemView.findViewById(R.id.cardCall);
        }
    }
}
