package kashyap.chandan.wepeople;

import kashyap.chandan.wepeople.ServiceProvider.ProviderEnquirtResponse;
import kashyap.chandan.wepeople.ServiceProvider.ProviderRegistrationResponse;
import kashyap.chandan.wepeople.customer.BecomeProviderResponse;
import kashyap.chandan.wepeople.customer.CustomerRegistrationResponse;
import kashyap.chandan.wepeople.customer.EnquiryResponse;
import kashyap.chandan.wepeople.customer.MyEnquiryResponse;
import kashyap.chandan.wepeople.customer.ProviderListResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface RequestInterface {
    @FormUrlEncoded
    @Headers("X-API-KEY:provider@123")
    @POST("loginservices/login")
    Call<LoginResponse>login(@Field("email_phone") String username, @Field("password") String password);

    @FormUrlEncoded
    @Headers("X-API-KEY:provider@123")
    @POST("forgetpasswordservices/forget_password")
    Call<ForgetPasswordResponse>forgetPassword(@Field("f_phone_email") String phone);

    @FormUrlEncoded
    @Headers("X-API-KEY:provider@123")
    @POST("forgetpasswordservices/reset_password")
    Call<ResetPasswordResponse>resetPassword(@Field("user_id") String id,@Field("otp") String otp,@Field("npass") String pass);

    @FormUrlEncoded
    @Headers("X-API-KEY:provider@123")
    @POST("loginservices/activation")
    Call<OtpVerificationResponse>accountVerification(@Field("otp") String otp, @Field("id") String id);

    @FormUrlEncoded
    @Headers("X-API-KEY:provider@123")
    @POST("loginservices/provider_registeration")
    Call<ProviderRegistrationResponse>providerRegistration(@Field("first_name") String fName,
                                            @Field("last_name") String lName,
                                            @Field("type_id") String tId,
                                            @Field("phone") String mobile,
                                            @Field("email") String email,
                                            @Field("description") String desc,
                                            @Field("area") String area,
                                            @Field("city_name") String city,
                                            @Field("state_name") String state,
                                            @Field("country_name")String country,
                                            @Field("pass")String password);
    @Multipart
    @Headers("X-API-KEY:provider@123")
    @POST("profileservices/update_profile")
    Call<ChangeProfileResponse>updateProfile(@Part("user_id") RequestBody uId,
                                                 @Part("first_name") RequestBody fName,
                                                           @Part("last_name") RequestBody lName,
                                                           @Part("type_id") RequestBody tId,
                                                           @Part("phone") RequestBody mobile,
                                                           @Part("email") RequestBody email,
                                                           @Part("area") RequestBody area,
                                                           @Part("city_name") RequestBody city,
                                                           @Part("state_name") RequestBody state,
                                                           @Part("country_name")RequestBody country,
                                             @Part MultipartBody.Part profileImage
                                             );



    @FormUrlEncoded
    @Headers("X-API-KEY:provider@123")
    @POST("loginservices/customer_registeration")
    Call<CustomerRegistrationResponse>userRegistration(@Field("first_name") String fName,
                                                       @Field("last_name") String lName,
                                                       @Field("phone") String mobile,
                                                       @Field("email") String email,
                                                       @Field("description") String desc,
                                                       @Field("area") String area,
                                                       @Field("city_name") String city,
                                                       @Field("state_name") String state,
                                                       @Field("country_name")String country,
                                                       @Field("pass")String password);

    @FormUrlEncoded
    @Headers("X-API-KEY:provider@123")
    @POST("serviceproviderservices/customer_myenqury")
    Call<MyEnquiryResponse>customer_My_Enquiry( @Field("customer_id") String id);

    @FormUrlEncoded
    @Headers("X-API-KEY:provider@123")
    @POST("serviceproviderservices/customer_enquries")
    Call<ProviderEnquirtResponse>provider_My_Enquiry(@Field("provider_id") String id);

    @FormUrlEncoded
    @Headers("X-API-KEY:provider@123")
    @POST("serviceproviderservices/be_a_service_provider")
    Call<BecomeProviderResponse>becomeProvider(@Field("customer_id") String id,
                                               @Field("type_id") String type_id,
                                               @Field("description") String description);


    @FormUrlEncoded
    @Headers("X-API-KEY:provider@123")
    @POST("profileservices/change_password")
    Call<ChangePasswordResponse>changePassword(@Field("user_id") String id,
                                               @Field("opwd") String oldpass,
                                               @Field("npwd") String newpass);


    @FormUrlEncoded
    @Headers("X-API-KEY:provider@123")
    @POST("customerservices/make_enqury")
    Call<EnquiryResponse>makeEnquiry(@Field("customer_id") String c_id,
                                     @Field("provider_id") String p_Id);


    @Headers("X-API-KEY:provider@123")
    @GET("valuesservices/active_service_types_list")
    Call<ServiceTypeResponse> serviceTypeList();

    @Headers("X-API-KEY:provider@123")
    @GET("serviceproviderservices/provider_list")
    Call<ProviderListResponse> providerList();

}
