package kashyap.chandan.wepeople;

import android.view.View;

public interface CustomItemClickListener {
    public void onItemClick(View v, String id, String value);
}
