package kashyap.chandan.wepeople.ServiceProvider;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.wepeople.CustomItemClickListener;
import kashyap.chandan.wepeople.R;
import kashyap.chandan.wepeople.ServiceTypeResponse;

public class ServiceTypeAdapter extends RecyclerView.Adapter<ServiceTypeAdapter.MyViewHolder> {
    Context context;
    List<ServiceTypeResponse.DataBean> serviceTypeList;
    Dialog serviceDialog;
    CustomItemClickListener listener;
    private int mSelectedItem = -1;
    public ServiceTypeAdapter(Context context, List<ServiceTypeResponse.DataBean> serviceTypeList, Dialog serviceDialog, CustomItemClickListener listener) {
   this.context=context;
   this.listener=listener;
   this.serviceDialog=serviceDialog;
   this.serviceTypeList=serviceTypeList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.option,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.radioselect.setChecked(position == mSelectedItem);
        holder.tvService.setText(serviceTypeList.get(position).getType());
        TextView ok= serviceDialog.findViewById(R.id.ok);
        TextView dialogheader=serviceDialog.findViewById(R.id.dialogheader);
        dialogheader.setText("Services");
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectedItem==-1)
                { Toast.makeText(context, "Select Service", Toast.LENGTH_SHORT).show(); }
                else
                { serviceDialog.dismiss(); }
            }
        });


    }

    @Override
    public int getItemCount() {
        return serviceTypeList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvService;
        RadioButton radioselect;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvService=itemView.findViewById(R.id.item);
            radioselect=itemView.findViewById(R.id.selected);
View.OnClickListener clickListener=new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        mSelectedItem = getAdapterPosition();
        notifyDataSetChanged();
        listener.onItemClick(view,serviceTypeList.get(mSelectedItem).getId(),serviceTypeList.get(mSelectedItem).getType());
    }
};
            itemView.setOnClickListener(clickListener);
            radioselect.setOnClickListener(clickListener);
        }
    }
}
