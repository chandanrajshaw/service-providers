package kashyap.chandan.wepeople.ServiceProvider;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

import kashyap.chandan.wepeople.ApiClient;
import kashyap.chandan.wepeople.ApiError;
import kashyap.chandan.wepeople.CustomItemClickListener;
import kashyap.chandan.wepeople.LocationAddress;
import kashyap.chandan.wepeople.Login;
import kashyap.chandan.wepeople.OtpVerification;
import kashyap.chandan.wepeople.R;
import kashyap.chandan.wepeople.RequestInterface;
import kashyap.chandan.wepeople.ServiceTypeResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class WorkerRegistration extends AppCompatActivity implements View.OnClickListener {
ImageView arrowGoBack;
TextView btnRegister,etServiceType;
LinearLayout serviceLayout;
    String latLngString;
    private final static int ALL_PERMISSIONS_RESULT = 101;
    FusedLocationProviderClient mFusedLocationClient;
    private double lng;
    private double lat;
    private String address;
    TextInputEditText etconfirmPassword,etpassword,etDescription,etemail,etmobile,etFullName,etAddress;
Dialog progressDialog,serviceDialog;
RecyclerView serviceRecycler;
String serviceTypeId=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_registration);
        init();
        arrowGoBack.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        etServiceType.setOnClickListener(this);

        if (!checkPermissions())
            requestPermissions();
        else {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(WorkerRegistration.this);
            getLastLocation();
        }
    }

    private void init() {
        serviceLayout=findViewById(R.id.serviceLayout);
        arrowGoBack=findViewById(R.id.arrowGoBack);
        btnRegister=findViewById(R.id.btnRegister);
        etAddress=findViewById(R.id.etAddress);
        etconfirmPassword=findViewById(R.id.etconfirmPassword);
        etpassword=findViewById(R.id.etpassword);
        etDescription=findViewById(R.id.etDescription);
        etemail=findViewById(R.id.etemail);
        etmobile=findViewById(R.id.etmobile);
        etServiceType=findViewById(R.id.etServiceType);
        etFullName=findViewById(R.id.etFullName);
        serviceDialog=new Dialog(WorkerRegistration.this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.serviceLayout:
            serviceType();
                break;
            case R.id.arrowGoBack:
                finish();
                break;
            case R.id.btnRegister:
                providerRegistration();
                break;
            case R.id.etServiceType:
                serviceType();
                break;

        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case ALL_PERMISSIONS_RESULT:
                int length= grantResults.length;
                if (grantResults.length>0)
                {
                    for (int i=0;i<length;i++)
                    {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            // Granted. Start getting the location information
                        }
                        else if(grantResults[i]==PackageManager.PERMISSION_DENIED) {
                            {
                                final AlertDialog.Builder builder=new AlertDialog.Builder(WorkerRegistration.this,R.style.AlertDialogTheme);
                                builder.setTitle("Notice");
                                builder.setMessage("You Had Denied the Necessary Permissions.Please Go to app Setting and give All the permissions");
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent=new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                                        intent.setData(Uri.parse("package:" +getApplicationContext().getPackageName()));
                                        startActivity(intent);
                                    }
                                });
                                builder.show();
                                break;
                            }

                        }

                    }

                }
                else {
                    getLastLocation();
                }
        }
    }
    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }
    @SuppressLint("MissingPermission")
    private void getLastLocation(){
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {
                                    lat=location.getLatitude();
                                    lng=location.getLongitude();
                                    Toast.makeText(WorkerRegistration.this, lat+" "+lng, Toast.LENGTH_SHORT).show();
                                    LocationAddress locationAddress = new LocationAddress();
                                    locationAddress.getAddressFromLocation(location.getLatitude(), location.getLongitude(),
                                           WorkerRegistration.this, new GeocoderHandler());
                                }
                            }
                        }
                );
            } else {
                Toast.makeText(WorkerRegistration.this, "Turn on location", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                WorkerRegistration.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                ALL_PERMISSIONS_RESULT
        );
    }
    @SuppressLint("MissingPermission")
    private void requestNewLocationData(){

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(WorkerRegistration.this);
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }
    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            lat=mLastLocation.getLatitude();
            lng=mLastLocation.getLongitude();

        }
    };
    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(WorkerRegistration.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(WorkerRegistration.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }
    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
            address=locationAddress;
            etAddress.setText(locationAddress);
        }
    }
    void providerRegistration()
    {
        String name=etFullName.getText().toString().trim();
//        String serviceId=serviceTypeId;
        String phone=etmobile.getText().toString();
        String email=etemail.getText().toString();
        String address=etAddress.getText().toString();
        String Description=etDescription.getText().toString();
        String pass=etpassword.getText().toString();
        String conPass=etconfirmPassword.getText().toString();
        if (name.isEmpty()&&serviceTypeId==null&&phone.isEmpty()&&email.isEmpty()&&address.isEmpty()&&Description.isEmpty()
        &&pass.isEmpty()&&conPass.isEmpty())
            Toast.makeText(this, "Enter All the Fields", Toast.LENGTH_SHORT).show();
else if (name.isEmpty())
            Toast.makeText(this, "Enter Your Full Name", Toast.LENGTH_SHORT).show();
else if (serviceTypeId==null)
            Toast.makeText(this, "Select Service Type", Toast.LENGTH_SHORT).show();
else if (phone.isEmpty()||phone.length()!=10)
            Toast.makeText(this, "Enter Valid Phone Number", Toast.LENGTH_SHORT).show();
else if (email.isEmpty())
            Toast.makeText(this, "Please Enter Valid Mail", Toast.LENGTH_SHORT).show();
else if (Description.isEmpty())
            Toast.makeText(this, "Enter Description Field", Toast.LENGTH_SHORT).show();
else if (pass.isEmpty())
            Toast.makeText(this, "Enter Password", Toast.LENGTH_SHORT).show();
else if (conPass.isEmpty())
            Toast.makeText(this, "Confirm Password", Toast.LENGTH_SHORT).show();
else if (!pass.equals(conPass))
            Toast.makeText(this, "Password Miss Match,Enter Both Password Field Carefully", Toast.LENGTH_SHORT).show();
else
        {
            String addressline[]= TextUtils.split(address,",");
            String area=addressline[0];
            String city=addressline[1];
            String state=addressline[2];
            String country=addressline[3];
//            Toast.makeText(this, ""+area+"\n"+city+"\n"+state+"\n"+country, Toast.LENGTH_SHORT).show();
            progressDialog=new Dialog(WorkerRegistration.this);
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            RequestInterface requestInterface= ApiClient.getClient().create(RequestInterface.class);
           Call<ProviderRegistrationResponse>call=requestInterface.providerRegistration(name,"",serviceTypeId,
                   phone,email,Description,area,city,state,country,conPass);
           call.enqueue(new Callback<ProviderRegistrationResponse>() {
               @Override
               public void onResponse(Call<ProviderRegistrationResponse> call, Response<ProviderRegistrationResponse> response) {
                   if (response.code() == 200)
                   {
                        progressDialog.dismiss();
                        String id=response.body().getUser_id();
                        Toast.makeText(WorkerRegistration.this, "Registration Successfull", Toast.LENGTH_SHORT).show();
                        Intent otpVerificationIntent=new Intent(WorkerRegistration.this, OtpVerification.class);
                        otpVerificationIntent.putExtra("id",id);
                       startActivity(otpVerificationIntent);
                   }
                   else
                   {
                       progressDialog.dismiss();
                       Converter<ResponseBody, ApiError> converter =
                               ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                       ApiError error;
                       try {
                           error = converter.convert(response.errorBody());
                           ApiError.StatusBean status=error.getStatus();
                           Toast.makeText(WorkerRegistration.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                       } catch (IOException e) { e.printStackTrace(); }
                   }
               }

               @Override
               public void onFailure(Call<ProviderRegistrationResponse> call, Throwable t) {

               }
           });

        }
    }
    void serviceType()
    {
        progressDialog=new Dialog(WorkerRegistration.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        RequestInterface requestInterface= ApiClient.getClient().create(RequestInterface.class);
        Call<ServiceTypeResponse> call=requestInterface.serviceTypeList();
        call.enqueue(new Callback<ServiceTypeResponse>() {
            @Override
            public void onResponse(Call<ServiceTypeResponse> call, Response<ServiceTypeResponse> response) {
if (response.code()==200)
{
    progressDialog.dismiss();
    List<ServiceTypeResponse.DataBean> serviceTypeList=response.body().getData();
    serviceDialog=new Dialog(WorkerRegistration.this);
    serviceDialog.setContentView(R.layout.dropdowndialog);
    DisplayMetrics metrics = getResources().getDisplayMetrics();
    int width = metrics.widthPixels;
    serviceDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
    serviceRecycler = serviceDialog.findViewById(R.id.recycleroption);
    serviceRecycler.setLayoutManager(new LinearLayoutManager(WorkerRegistration.this, LinearLayoutManager.VERTICAL, false));
serviceRecycler.setAdapter(new ServiceTypeAdapter(WorkerRegistration.this,serviceTypeList,serviceDialog, new CustomItemClickListener() {
    @Override
    public void onItemClick(View v, String id, String value) {
etServiceType.setText(value);
serviceTypeId=id;
    }
}
));
serviceDialog.show();
}
else
{
    progressDialog.dismiss();
    Converter<ResponseBody, ApiError> converter =
            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
    ApiError error;
    try {
        error = converter.convert(response.errorBody());
        ApiError.StatusBean status=error.getStatus();
        Toast.makeText(WorkerRegistration.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
    } catch (IOException e) { e.printStackTrace(); }
}
            }

            @Override
            public void onFailure(Call<ServiceTypeResponse> call, Throwable t) {
progressDialog.dismiss();
                Snackbar.make(WorkerRegistration.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();

            }
        });
    }
}