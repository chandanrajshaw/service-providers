package kashyap.chandan.wepeople.ServiceProvider;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.wepeople.ApiClient;
import kashyap.chandan.wepeople.ApiError;
import kashyap.chandan.wepeople.CustomItemClickListener;
import kashyap.chandan.wepeople.Login;
import kashyap.chandan.wepeople.LoginResponse;
import kashyap.chandan.wepeople.R;
import kashyap.chandan.wepeople.RequestInterface;
import kashyap.chandan.wepeople.SharedPreferences;
import kashyap.chandan.wepeople.customer.CustomerDashBoard;
import kashyap.chandan.wepeople.customer.MyEnquiryAdapter;
import kashyap.chandan.wepeople.customer.MyEnquiryResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ProviderDashBoard extends AppCompatActivity {
    Toolbar toolbar;
    DrawerLayout drawerlayout;
    TextView proname,phone,tvTitle;
    Intent intent;
    RecyclerView enquiryList;
    CircleImageView   ivProfile,navheaderpropic;
    LinearLayout logoutLayout,profile,homeLayout;
    SharedPreferences preferences;
    Dialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_dash_board);
        init();
        intent=getIntent();
        Bundle bundle=intent.getBundleExtra("bundle");
        LoginResponse.DataBean userData= (LoginResponse.DataBean) bundle.getSerializable("data");

        tvTitle.setText("Welcome "+preferences.getFName());
        proname.setText(preferences.getFName()+" "+preferences.getLName());
        phone.setText(preferences.getPhone());
//        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        toolbar.setNavigationIcon(R.drawable.ic_view_list);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { drawerlayout.openDrawer(Gravity.LEFT); } });
        if (!preferences.getImage().isEmpty())
        {
            Picasso.get().load("http://www.igranddeveloper.xyz/sprovider/"+preferences.getImage()).placeholder(R.drawable.loading).error(R.drawable.servicelogo).into(navheaderpropic);

        }
        ivProfile.setVisibility(View.GONE);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ProviderDashBoard.this,EditProfile.class);
                startActivity(intent);
                drawerlayout.closeDrawer(GravityCompat.START);
            }
        });
        logoutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.sessionEnd();
                Intent intent=new Intent(ProviderDashBoard.this, Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        homeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerlayout.closeDrawer(GravityCompat.START);
            }
        });
    }

    private void init() {
        homeLayout=findViewById(R.id.homeLayout);
        profile=findViewById(R.id.profileLay);
        logoutLayout=findViewById(R.id.logoutLayout);
        navheaderpropic=findViewById(R.id.navheaderpropic);
        ivProfile=findViewById(R.id.ivProfile);
        enquiryList=findViewById(R.id.enquiryList);
        toolbar=findViewById(R.id.toolbar);
        drawerlayout=findViewById(R.id.drawerLayout);
        proname=findViewById(R.id.tvproname);
        phone=findViewById(R.id.adminphone);
        tvTitle=findViewById(R.id.tvTitle);
        preferences=new SharedPreferences(ProviderDashBoard.this);

    }
    void enquiryList()
    {
        progressDialog=new Dialog(ProviderDashBoard.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        RequestInterface requestInterface= ApiClient.getClient().create(RequestInterface.class);
        Call<ProviderEnquirtResponse> call=requestInterface.provider_My_Enquiry(preferences.getId());
        call.enqueue(new Callback<ProviderEnquirtResponse>() {
            @Override
            public void onResponse(Call<ProviderEnquirtResponse> call, Response<ProviderEnquirtResponse> response) {
                if (response.code()==200)
                {
                    progressDialog.dismiss();
                    List<ProviderEnquirtResponse.DataBean> dataList=response.body().getData();
                    enquiryList.setLayoutManager(new LinearLayoutManager(ProviderDashBoard.this,LinearLayoutManager.VERTICAL,false));
                    enquiryList.setAdapter(new ProviderEnquiryAdapter(ProviderDashBoard.this,dataList, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, String id, String value) {

                        }
                    }));
                }
                else
                {
                    progressDialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(ProviderDashBoard.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<ProviderEnquirtResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ProviderDashBoard.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        enquiryList();
    }
}