package kashyap.chandan.wepeople.ServiceProvider;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.wepeople.ApiClient;
import kashyap.chandan.wepeople.ApiError;
import kashyap.chandan.wepeople.ChangePassword;
import kashyap.chandan.wepeople.ChangeProfileResponse;
import kashyap.chandan.wepeople.CustomItemClickListener;
import kashyap.chandan.wepeople.LocationAddress;
import kashyap.chandan.wepeople.Login;
import kashyap.chandan.wepeople.OtpVerification;
import kashyap.chandan.wepeople.R;
import kashyap.chandan.wepeople.RequestInterface;
import kashyap.chandan.wepeople.ServiceTypeResponse;
import kashyap.chandan.wepeople.SharedPreferences;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.http.Part;

public class EditProfile extends AppCompatActivity implements View.OnClickListener {
    TextView changePassword, btnUpdate,etServiceType,providerview;
    ImageView arrowGoBack;
    LinearLayout serviceLayout;
    SharedPreferences preferences;
    Dialog progressDialog, dialog, serviceDialog;
    RecyclerView serviceRecycler;
    TextInputEditText etFName, etLName, etmobile, etemail, etAddress;
    private final static int ALL_PERMISSIONS_RESULT = 101;
    FusedLocationProviderClient mFusedLocationClient;
    private double lng;
    private double lat;
    String serviceTypeId = null;
    CircleImageView profileImage;
    Bitmap bitmap, converetdImage;
    RelativeLayout profileImagelayout;
    File image = null;
    String picturePath, imagePic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        init();
        if (!preferences.getImage().isEmpty())
            Picasso.get().load("http://www.igranddeveloper.xyz/sprovider/"+preferences.getImage()).placeholder(R.drawable.loading).error(R.drawable.servicelogo).into(profileImage);

        etLName.setText(preferences.getLName());
        etFName.setText(preferences.getFName());
        etemail.setText(preferences.getemail());
        etmobile.setText(preferences.getPhone());
        changePassword.setOnClickListener(this);
        arrowGoBack.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
        if (preferences.getRole().equalsIgnoreCase("service_provider")) {
            serviceLayout.setVisibility(View.VISIBLE);
            providerview.setVisibility(View.VISIBLE);
        } else {
            serviceLayout.setVisibility(View.GONE);
            providerview.setVisibility(View.GONE);
        }
        if (!checkPermissions())
            requestPermissions();
        else {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(EditProfile.this);
            getLastLocation();
        }

        if (picturePath != null && !picturePath.isEmpty() && !picturePath.equals("null"))
        {
            Picasso.get().load(imagePic).into(profileImage);
            bitmap = ((BitmapDrawable) profileImage.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);
        }
        else {}
        profileImagelayout.setOnClickListener(this);
        serviceLayout.setOnClickListener(this);
        etServiceType.setOnClickListener(this);
    }

    private void init() {
        providerview=findViewById(R.id.providerview);
        etServiceType = findViewById(R.id.etServiceType);
        profileImage = findViewById(R.id.profileImage);
        profileImagelayout = findViewById(R.id.profileImagelayout);
        preferences = new SharedPreferences(EditProfile.this);
        changePassword = findViewById(R.id.changePassword);
        arrowGoBack = findViewById(R.id.arrowGoBack);
        serviceLayout = findViewById(R.id.serviceLayout);
        etFName = findViewById(R.id.etFName);
        etLName = findViewById(R.id.etLName);
        etmobile = findViewById(R.id.etmobile);
        etemail = findViewById(R.id.etemail);
        etAddress = findViewById(R.id.etAddress);
        btnUpdate = findViewById(R.id.btnUpdate);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.changePassword:
                Intent changePasswordIntent = new Intent(EditProfile.this, ChangePassword.class);
                startActivity(changePasswordIntent);
                break;
            case R.id.arrowGoBack:
                finish();
                break;
            case R.id.etServiceType:
               serviceType();
                break;
            case R.id.profileImagelayout:
                LinearLayout camera, folder;
                dialog = new Dialog(EditProfile.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                DisplayMetrics metrics = getResources().getDisplayMetrics();
                int width = metrics.widthPixels;
                dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);
                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);*/
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 100);
                        dialog.dismiss();


                    }
                });
                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 101);
                        dialog.dismiss();
                    }
                });
                break;

               case  R.id.btnUpdate:
                   updateProfile();
                   break;

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                int length = grantResults.length;
                if (grantResults.length > 0) {
                    for (int i = 0; i < length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            // Granted. Start getting the location information
                        } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                            {
                                final AlertDialog.Builder builder = new AlertDialog.Builder(EditProfile.this, R.style.AlertDialogTheme);
                                builder.setTitle("Notice");
                                builder.setMessage("You Had Denied the Necessary Permissions.Please Go to app Setting and give All the permissions");
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent = new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                                        intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
                                        startActivity(intent);
                                    }
                                });
                                builder.show();
                                break;
                            }

                        }

                    }

                } else {
                    getLastLocation();
                }
        }
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }

    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {
                                    lat = location.getLatitude();
                                    lng = location.getLongitude();
                                    Toast.makeText(EditProfile.this, lat + " " + lng, Toast.LENGTH_SHORT).show();
                                    LocationAddress locationAddress = new LocationAddress();
                                    locationAddress.getAddressFromLocation(location.getLatitude(), location.getLongitude(),
                                            EditProfile.this, new GeocoderHandler());
                                }
                            }
                        }
                );
            } else {
                Toast.makeText(EditProfile.this, "Turn on location", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                EditProfile.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                ALL_PERMISSIONS_RESULT
        );
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //  if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

        //     imagepath=selectedImage.getPath();


        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                profileImage.setImageBitmap(converetdImage);
                profileImage.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            profileImage.setImageBitmap(converetdImage);
            profileImage.setVisibility(View.VISIBLE);
            image = new File(Environment.getExternalStorageDirectory(), "Profile.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

    @SuppressLint("MissingPermission")
    private void requestNewLocationData() {

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(EditProfile.this);
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            lat = mLastLocation.getLatitude();
            lng = mLastLocation.getLongitude();

        }
    };

    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(EditProfile.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(EditProfile.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
            etAddress.setText(locationAddress);
        }
    }

    void updateProfile() {
        String name = etFName.getText().toString().trim();
        String lname = etLName.getText().toString().trim();
        String phone = etmobile.getText().toString();
        String email = etemail.getText().toString();
        String address = etAddress.getText().toString();
        if (name.isEmpty() && serviceTypeId == null && phone.isEmpty() && email.isEmpty() && address.isEmpty())
            Toast.makeText(this, "Enter All the Fields", Toast.LENGTH_SHORT).show();
        else if (name.isEmpty())
            Toast.makeText(this, "Enter Your Full Name", Toast.LENGTH_SHORT).show();
//        else if (serviceTypeId==null)
//            Toast.makeText(this, "Select Service Type", Toast.LENGTH_SHORT).show();
        else if (phone.isEmpty() || phone.length() != 10)
            Toast.makeText(this, "Enter Valid Phone Number", Toast.LENGTH_SHORT).show();
        else if (email.isEmpty())
            Toast.makeText(this, "Please Enter Valid Mail", Toast.LENGTH_SHORT).show();
        else {
            String addressline[] = TextUtils.split(address, ",");
            String area = addressline[0];
            String city = addressline[1];
            String state = addressline[2];
            String country = addressline[3];
            Toast.makeText(this, "" + area + "\n" + city + "\n" + state + "\n" + country, Toast.LENGTH_SHORT).show();
            RequestInterface requestInterface = ApiClient.getClient().create(RequestInterface.class);
            RequestBody id = RequestBody.create(MediaType.parse("multipart/form-data"), preferences.getId());
            RequestBody fname = RequestBody.create(MediaType.parse("multipart/form-data"), name);
            RequestBody lastname = RequestBody.create(MediaType.parse("multipart/form-data"), lname);
            RequestBody mail = RequestBody.create(MediaType.parse("multipart/form-data"), email);
            RequestBody mob = RequestBody.create(MediaType.parse("multipart/form-data"), phone);
            RequestBody pArea = RequestBody.create(MediaType.parse("multipart/form-data"), area);
            RequestBody pCity = RequestBody.create(MediaType.parse("multipart/form-data"), city);
            RequestBody pstate = RequestBody.create(MediaType.parse("multipart/form-data"), state);
            RequestBody pcountry = RequestBody.create(MediaType.parse("multipart/form-data"), country);
            MultipartBody.Part profileimg = null;
            if (image != null) {
                RequestBody img1 = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                profileimg = MultipartBody.Part.createFormData("image", image.getName(), img1);
            }
                if (preferences.getRole().equalsIgnoreCase("service_provider")) {
                     if (serviceTypeId == null)
                        Toast.makeText(this, "Select Service Type", Toast.LENGTH_SHORT).show();
                        else {
                        progressDialog = new Dialog(EditProfile.this);
                        progressDialog.setContentView(R.layout.loadingdialog);
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        RequestBody typeId = RequestBody.create(MediaType.parse("multipart/form-data"), serviceTypeId);
                        Call<ChangeProfileResponse> call = requestInterface.updateProfile(id,fname,lastname,typeId,mob,mail,pArea,pCity,pstate,pcountry,profileimg);
                        call.enqueue(new Callback<ChangeProfileResponse>() {
                            @Override
                            public void onResponse(Call<ChangeProfileResponse> call, Response<ChangeProfileResponse> response) {
                                if (response.code() == 200) {
                                progressDialog.dismiss();
                                    Toast.makeText(EditProfile.this, "Profile Updated", Toast.LENGTH_SHORT).show();
                                    Intent intent=new Intent(EditProfile.this, Login.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                                else {
                                    progressDialog.dismiss();
                                    Toast.makeText(EditProfile.this, "Profile Updation Failed", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ChangeProfileResponse> call, Throwable t) {
                                progressDialog.dismiss();
                                Toast.makeText(EditProfile.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
                else {
                    progressDialog = new Dialog(EditProfile.this);
                    progressDialog.setContentView(R.layout.loadingdialog);
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    serviceTypeId = "";
                    progressDialog = new Dialog(EditProfile.this);
                    progressDialog.setContentView(R.layout.loadingdialog);
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    RequestBody typeId = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                    Call<ChangeProfileResponse> call = requestInterface.updateProfile(id,fname,lastname,typeId,mob,mail,pArea,pCity,pstate,pcountry,profileimg);
                    call.enqueue(new Callback<ChangeProfileResponse>() {
                        @Override
                        public void onResponse(Call<ChangeProfileResponse> call, Response<ChangeProfileResponse> response) {
                            if (response.code() == 200) {
                                progressDialog.dismiss();
                                Toast.makeText(EditProfile.this, "Profile Updated", Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(EditProfile.this, Login.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                            else {
                                progressDialog.dismiss();
                                Toast.makeText(EditProfile.this, "Profile Updation Failed", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ChangeProfileResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(EditProfile.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }
        }




    private Bitmap getResizedBitmap (Bitmap image,int maxSize){

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
    void serviceType()
    {
        progressDialog = new Dialog(EditProfile.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        RequestInterface requestInterface = ApiClient.getClient().create(RequestInterface.class);
        Call<ServiceTypeResponse> call = requestInterface.serviceTypeList();
        call.enqueue(new Callback<ServiceTypeResponse>() {
            @Override
            public void onResponse(Call<ServiceTypeResponse> call, Response<ServiceTypeResponse> response) {
                if (response.code() == 200) {
                    progressDialog.dismiss();
                    List<ServiceTypeResponse.DataBean> serviceTypeList = response.body().getData();
                    serviceDialog = new Dialog(EditProfile.this);
                    serviceDialog.setContentView(R.layout.dropdowndialog);
                    DisplayMetrics metrics = getResources().getDisplayMetrics();
                    int width = metrics.widthPixels;
                    serviceDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                    serviceRecycler = serviceDialog.findViewById(R.id.recycleroption);
                    serviceRecycler.setLayoutManager(new LinearLayoutManager(EditProfile.this, LinearLayoutManager.VERTICAL, false));
                    serviceRecycler.setAdapter(new ServiceTypeAdapter(EditProfile.this, serviceTypeList, serviceDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, String id, String value) {
                            etServiceType.setText(value);
                            serviceTypeId = id;
                        }
                    }
                    ));
                    serviceDialog.show();
                } else {
                    progressDialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class, new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status = error.getStatus();
                        Toast.makeText(EditProfile.this, "" + status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ServiceTypeResponse> call, Throwable t) {
                progressDialog.dismiss();
                Snackbar.make(EditProfile.this.getWindow().getDecorView().findViewById(android.R.id.content), "" + t.getMessage(), Snackbar.LENGTH_SHORT).show();

            }
        });
    }

}