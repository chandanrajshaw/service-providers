package kashyap.chandan.wepeople.ServiceProvider;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.wepeople.CustomItemClickListener;
import kashyap.chandan.wepeople.R;

public class ProviderEnquiryAdapter extends RecyclerView.Adapter<ProviderEnquiryAdapter.MyViewHolder> {
    Context context;
    List<ProviderEnquirtResponse.DataBean> dataList;
    CustomItemClickListener listener;
    private int mSelectedItem = -1;
    public ProviderEnquiryAdapter(Context context, List<ProviderEnquirtResponse.DataBean> dataList, CustomItemClickListener listener) {
    this.context=context;
    this.dataList=dataList;
    this.listener=listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.enquiry_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
    holder.tvProviderName.setText(dataList.get(position).getFirst_name()+" "+dataList.get(position).getLast_name());
    holder.icon2.setImageResource(R.drawable.ic_location);
    holder.tvProvidingFor.setText(dataList.get(position).getArea()+","+dataList.get(position).getCity_name());
    holder.dateandTime.setText(dataList.get(position).getDate_time());
        Picasso.get().load("http://www.igranddeveloper.xyz/sprovider/"+dataList.get(position).getImage()).placeholder(R.drawable.loading).error(R.drawable.servicelogo).into(holder.customerImage);
        holder.callLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos=holder.getAdapterPosition();
                String mob=dataList.get(pos).getPhone();
                Uri u = Uri.fromParts("tel",mob,null);
                Intent i = new Intent(Intent.ACTION_DIAL, u);
                context.startActivity(i);
            }
        });
    }
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CardView callLayout;
        TextView tvProviderName,tvProvidingFor, dateandTime;
        TextView call;
        ImageView icon2;
        CircleImageView customerImage;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            customerImage=itemView.findViewById(R.id.customerImage);
            dateandTime =itemView.findViewById(R.id.tvProviderAddress);
            tvProvidingFor=itemView.findViewById(R.id.tvProvidingFor);
            tvProviderName=itemView.findViewById(R.id.tvProviderName);
            icon2=itemView.findViewById(R.id.icon2);
            call=itemView.findViewById(R.id.call);
            callLayout=itemView.findViewById(R.id.callLayout);
            callLayout.setVisibility(View.VISIBLE);
            View.OnClickListener clickListener=new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    listener.onItemClick(view,dataList.get(mSelectedItem).getId(),dataList.get(mSelectedItem).getFirst_name()+""+dataList.get(mSelectedItem).getLast_name());
                }
            };
            itemView.setOnClickListener(clickListener);
        }
    }
}
