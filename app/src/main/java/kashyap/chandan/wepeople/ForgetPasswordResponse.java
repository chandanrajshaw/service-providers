package kashyap.chandan.wepeople;

import java.io.Serializable;

public class ForgetPasswordResponse implements Serializable {

    /**
     * status : {"code":200,"message":"Otp sent to Your Registed Email"}
     * user_id : 3
     */

    private StatusBean status;
    private String user_id;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Otp sent to Your Registed Email
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
