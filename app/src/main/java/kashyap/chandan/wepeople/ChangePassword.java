package kashyap.chandan.wepeople;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.wepeople.ServiceProvider.EditProfile;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ChangePassword extends AppCompatActivity implements View.OnClickListener {
  ImageView arrowGoBack;
  TextInputEditText etConfirmPassword,etNewPassword,etOldPass;
  TextView btnChangePassword;
  SharedPreferences preferences;
  Dialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        init();
        arrowGoBack.setOnClickListener(this);
        btnChangePassword.setOnClickListener(this);

    }
    private void init() {
        preferences=new SharedPreferences(ChangePassword.this);
        btnChangePassword=findViewById(R.id.btnChangePassword);
        arrowGoBack=findViewById(R.id.arrowGoBack);
        etConfirmPassword=findViewById(R.id.etConfirmPassword);
        etNewPassword=findViewById(R.id.etNewPassword);
        etOldPass=findViewById(R.id.etOldPass);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.arrowGoBack:
                finish();
                break;
            case R.id.btnChangePassword:
                changePassword();
                break;
        }
    }
    void changePassword()
    {
       String oldPass=etOldPass.getText().toString();
       String newPassword=etNewPassword.getText().toString();
       String conPass=etConfirmPassword.getText().toString();
       if (oldPass.isEmpty()&&newPassword.isEmpty()&&conPass.isEmpty())
           Toast.makeText(this, "Enter All the Fields", Toast.LENGTH_SHORT).show();
      else if (oldPass.isEmpty())
           Toast.makeText(this, "Enter Old Password", Toast.LENGTH_SHORT).show();
      else if (newPassword.isEmpty())
           Toast.makeText(this, "Enter New Password", Toast.LENGTH_SHORT).show();
      else if (conPass.isEmpty())
           Toast.makeText(this, "Enter Confirm Password", Toast.LENGTH_SHORT).show();
      else if (!newPassword.equals(conPass))
           Toast.makeText(this, "Password Miss Match!! Check Confirm Password", Toast.LENGTH_SHORT).show();
      else
       {
           progressDialog=new Dialog(ChangePassword.this);
           progressDialog.setContentView(R.layout.loadingdialog);
           progressDialog.setCancelable(false);
           progressDialog.show();
           RequestInterface requestInterface=ApiClient.getClient().create(RequestInterface.class);
           Call<ChangePasswordResponse>call=requestInterface.changePassword(preferences.getId(),oldPass,newPassword);
           call.enqueue(new Callback<ChangePasswordResponse>() {
               @Override
               public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                   if (response.code()==200)
                   {
                       progressDialog.dismiss();
                       preferences.sessionEnd();
                       Toast.makeText(ChangePassword.this, "Password Changed Successfully", Toast.LENGTH_SHORT).show();
                       Intent intent=new Intent(ChangePassword.this,Login.class);
                       intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                       startActivity(intent);
                       finish();
                   }
                   else
                   {
                       progressDialog.dismiss();
                       Converter<ResponseBody, ApiError> converter =
                               ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                       ApiError error;
                       try {
                           error = converter.convert(response.errorBody());
                           ApiError.StatusBean status=error.getStatus();
                           Toast.makeText(ChangePassword.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                       } catch (IOException e) { e.printStackTrace(); }
                   }
               }

               @Override
               public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
progressDialog.dismiss();
                   Toast.makeText(ChangePassword.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
               }
           });
       }
    }
}