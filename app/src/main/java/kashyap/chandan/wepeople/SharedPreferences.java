package kashyap.chandan.wepeople;

import android.content.Context;

import static android.content.Context.MODE_PRIVATE;

public  class SharedPreferences {
    android.content.SharedPreferences sharedPreferences;
    android.content.SharedPreferences.Editor editor;
    Context context;
    boolean session=false;
    public SharedPreferences(Context context){
        this.context=context;
        this.sharedPreferences= this.context.getSharedPreferences("Login",MODE_PRIVATE);
        this.editor=sharedPreferences.edit();
    }
    public void putSharedPreference(String id,String fName,String lName,
                                                                         String pass,String phone,String email,String image,String
                                            area,String city,String state,String country,String role,boolean session)
    {
        this.session=session;
         editor.putString("firstName",fName);
        editor.putString("lastName",lName);
        editor.putString("password",pass);
        editor.putString("phone",phone);
        editor.putString("id",id);
        editor.putString("email",email);
        editor.putString("image",image);
        editor.putString("area",area);
        editor.putString("city",city);
        editor.putString("state",state);
        editor.putString("country",country);
        editor.putString("role",role);
        editor.putBoolean("session",session);
        editor.commit();
    }
    public void sessionEnd()
    {
        editor.clear();
        editor.commit();
    }
    public String getFName()
    {
        String fName=sharedPreferences.getString("firstName",null);
        return fName ;
    }
    public String getLName()
    {
        String lName=sharedPreferences.getString("lastName",null);
        return lName ;
    }
    public String getPhone()
    {
        String phone=sharedPreferences.getString("phone",null);
        return phone ;
    }
    public String getId()
    {
        String id=sharedPreferences.getString("id",null);
        return id ;
    }
    public String getemail()
    {
        String email=sharedPreferences.getString("email",null);
        return email ;
    }
    public String getPass()
    {
        String pass=sharedPreferences.getString("password",null);
        return pass ;
    }



    public String getImage()
    {
        String image=sharedPreferences.getString("image",null);
        return image ;
    }
    public String getArea()
    {
        String area=sharedPreferences.getString("area",null);
        return area ;
    }
    public String getCity()
    {
        String city=sharedPreferences.getString("city",null);
        return city ;
    }
    public String getState()
    {
        String state=sharedPreferences.getString("state",null);
        return state ;
    }
    public String getCountry()
    {
        String country=sharedPreferences.getString("country",null);
        return country ;
    }
    public String getRole()
    {
        String role=sharedPreferences.getString("role",null);
        return role ;
    }
    public boolean sessionSet()
    {
        return session;
    }

}
