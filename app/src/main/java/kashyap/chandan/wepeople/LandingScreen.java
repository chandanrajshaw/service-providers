package kashyap.chandan.wepeople;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.wepeople.customer.ProviderListResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class LandingScreen extends AppCompatActivity {
Toolbar toolbar;
RecyclerView serviceList;
CircleImageView ivProfile;
    private final static int ALL_PERMISSIONS_RESULT = 101;
    Dialog progressDialog;
    SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_screen);
        init();
        setSupportActionBar(toolbar);
        if (!checkPermissions())
        {requestPermissions();}
ivProfile.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(LandingScreen.this,Login.class);
        startActivity(intent);
    }
});
    }

    private void init() {
        preferences=new SharedPreferences(LandingScreen.this);
        toolbar=findViewById(R.id.toolbar);
        serviceList=findViewById(R.id.serviceList);
        ivProfile=findViewById(R.id.ivProfile);
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                LandingScreen.this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                ALL_PERMISSIONS_RESULT
        );
    }
    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(LandingScreen.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(LandingScreen.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                &&                ActivityCompat.checkSelfPermission(LandingScreen.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
&&                ActivityCompat.checkSelfPermission(LandingScreen.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED

        )
        {
            return true;
        }
        return false;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case ALL_PERMISSIONS_RESULT:
                int length= grantResults.length;
                if (grantResults.length>0)
                {
                    for (int i=0;i<length;i++)
                    {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            // Granted. Start getting the location information
                        }
                        else if(grantResults[i]==PackageManager.PERMISSION_DENIED) {
                            {
                                final AlertDialog.Builder builder=new AlertDialog.Builder(LandingScreen.this,R.style.AlertDialogTheme);
                                builder.setTitle("Notice");
                                builder.setMessage("You Had Denied the Necessary Permissions.Please Go to app Setting and give All the permissions");
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent=new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                                        intent.setData(Uri.parse("package:" +getApplicationContext().getPackageName()));
                                        startActivity(intent);
                                    }
                                });
                                builder.show();
                                break;
                            }

                        }

                    }

                }
                else {
//                    getLastLocation();
                }
        }
    }
    void serviceProviders()
    {
        progressDialog=new Dialog(LandingScreen.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        RequestInterface requestInterface= ApiClient.getClient().create(RequestInterface.class);
        Call<ProviderListResponse> call=requestInterface.providerList();
        call.enqueue(new Callback<ProviderListResponse>() {
            @Override
            public void onResponse(Call<ProviderListResponse> call, Response<ProviderListResponse> response) {
                if (response.code()==200)
                {
                    progressDialog.dismiss();
                    List<ProviderListResponse.DataBean> providerList=response.body().getData();
                    serviceList.setLayoutManager(new LinearLayoutManager(LandingScreen.this,LinearLayoutManager.VERTICAL,false));
                    serviceList.setAdapter(new LandingScreenAdapter(LandingScreen.this,providerList, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, String id, String value) {
//                        if (preferences.sessionSet())
//                        {}
                        }
                    }));
                }
                else
                {
                    progressDialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(LandingScreen.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }
                } }

            @Override
            public void onFailure(Call<ProviderListResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(LandingScreen.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        serviceProviders();
    }
}